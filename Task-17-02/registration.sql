-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2019 at 07:22 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

SET AUTOCOMMIT = 0;

START TRANSACTION;

SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */
;

/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */
;

/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */
;

/*!40101 SET NAMES utf8mb4 */
;

--
-- Database: `points`
--
--
-- Table structure for table `Information`
--
DROP TABLE IF EXISTS `Information`;
CREATE TABLE `Information` (
    `ID` int NOT NULL AUTO_INCREMENT,
    `Facebook` CHAR DEFAULT NULL,
    `Instagram` CHAR DEFAULT NULL,
    `Birth_Date` DATE,
    `Education` VARCHAR(100),
    `Bio` VARCHAR(255),
    `Price` int,
    `Points` int,
    `Tattoos` VARCHAR(100),
    `Scars` VARCHAR(100),
    `Talents` VARCHAR(100),
    `created_at` DATETIME,
    `User` CHAR,
    `Avatar` CHAR,
    `Model_Category` CHAR,
    `Nationality` CHAR,
    `Height` CHAR,
    `Skin_Tone` CHAR,
    `Weight` CHAR,
    `Hair_Type` CHAR,
    `Hair_Color` CHAR,
    `Eye_Color` CHAR,
    `Body_Shape` CHAR,
    `Gender` CHAR,
    `Languages` VARCHAR(100),
    `Auditions` VARCHAR(100),
    `Calenders` VARCHAR(100),
    `Work_Experiences` VARCHAR(100),

    PRIMARY KEY (`ID`),
    CONSTRAINT Links UNIQUE KEY (`Facebook`, `Instagram`)
);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */
;

/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */
;

/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */
;