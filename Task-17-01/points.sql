-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2019 at 07:22 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

SET AUTOCOMMIT = 0;

START TRANSACTION;

SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */
;

/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */
;

/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */
;

/*!40101 SET NAMES utf8mb4 */
;

--
-- Database: `points`
--
--
-- Table structure for table `Points`
--
DROP TABLE IF EXISTS `Points`;
CREATE TABLE `Points` (
    `ID` int NOT NULL AUTO_INCREMENT,
    `Name` varchar(100) NOT NULL,
    `Points` int DEFAULT 0,
    PRIMARY KEY (`ID`)
);
--
-- Dumping data for table `Points`
--
INSERT INTO `Points` (`Name`, `Points`) VALUES
('Omar Ashraf', 580),
('Abdelrahman Amr', 565),
('Mostafa Rafea', 715),
('Marco Mounir', 260),
('Sara Galal', 315),
('Omnia Mahmoud', 175),
('William', 430),
('Yousef Ibrahim', 440),
('Mohamed Baligh', 40),
('Ibrahim Morsei', 365),
('Karim Fekry', 255),
('Marwan', 470),
('Mohamed Tareq', 65),
('Omar AbdelRady', 715),
('Sherif Mahmoud', 225),
('Abdallah', 5);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */
;

/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */
;

/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */
;